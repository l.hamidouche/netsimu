package fr.lip6.util;

import peersim.core.Node;

import java.util.ArrayList;

/**
 * Created by magency on 20/06/2016.
 */
public class HammingDistance implements  Comparable<HammingDistance> {
    private NodeState node1;
    private NodeState node2;


    public HammingDistance(NodeState node1, NodeState node2) {
        this.node1 = node1;
        this.node2 = node2;
    }

    public NodeState getNode1() {
        return node1;
    }

    public void setNode1(NodeState node1) {
        this.node1 = node1;
    }

    public NodeState getNode2() {
        return node2;
    }

    public void setNode2(NodeState node2) {
        this.node2 = node2;
    }

    public int getScore(){
        return  node1.hamming(node2);
    }

    public String toString() {
        return ""+getScore();
    }
    public int compareTo(HammingDistance o) {
        return getScore() - o.getScore();
    }


}
