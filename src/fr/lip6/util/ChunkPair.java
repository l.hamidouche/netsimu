package fr.lip6.util;

/**
 * Created by magency on 15/06/2016.
 */
public class ChunkPair implements Comparable<ChunkPair> {
    public int index;
    public int count;

    public ChunkPair(int index,int count) {
        this.index = index;
        this.count = count;
    }

    public int compareTo(ChunkPair o) {
        if (count > o.count) {
            return 1 ;
        } else if (count < o.count ) {
            return -1;
        } else {
            return 0;
        }
    }

    public String toString() {
        return "<"+index+","+count+">";
    }

}
