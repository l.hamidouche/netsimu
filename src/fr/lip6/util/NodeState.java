package fr.lip6.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Node;

public class NodeState {

	// The states that a node can have
	public static final int Idle = 0 ;
	public static final int Downloading = 1 ;
	public static final int Sending = 2 ;
	public static final int WaitingForData = 3 ; // not implemented yet
	public static final int Sleeping = 4; // remove later
	public static final int Exchanging = 5; // Echange de données
	private int maxChunks;



	// Max Chunks to exchange with a node
	private static int MaxExchange; // Not used for the moment

	// The score defines whether the node has to request or to share with other nodes
	private double score;
	// The next time this node is going to be available
	private long nextAvailability;

	// Node types
	public static final int MobileNode = 1 ;
	//public static final int FastNode=2;

	// The class fields
	private int id;

	// Node state
	private int nodeType;

	// Sending
	private int state;

	// Check whether the data is a source
	private boolean source;

	// Check whether the node sleeps or not
	private boolean sleep;

	//Data state
	private boolean chunks[];

	public static int getWaitingForData() {
		return WaitingForData;
	}

	private long activityTime;
	private long inactivityTime;
	private long lastActiveTime;
	private long lastInctiveTime;

	private int groupId;


	public long getInactivityTime() {
		return inactivityTime;
	}

	public void setInactivityTime(long inactivityTime) {
		this.inactivityTime = inactivityTime;
	}

	public long getLastActiveTime() {
		return lastActiveTime;
	}

	public void setLastActiveTime(long lastActiveTime) {
		this.lastActiveTime = lastActiveTime;
	}

	public long getLastInctiveTime() {
		return lastInctiveTime;
	}

	public void setLastInctiveTime(long lastInctiveTime) {
		this.lastInctiveTime = lastInctiveTime;
	}

	public long getActivityTime() {
		return activityTime;
	}

	public void setActivityTime(long activityTime) {
		this.activityTime = activityTime;
	}

	private boolean finished;


	private boolean bigConsumer;


	// Boolean which requests the data to be sent
	private boolean  dataRequested;
	// Next destination
	private int nextDest;
	private int[] nextSend;

	// Chunk download strategy
	private int strategy;



	// Maximum number of receivers
	@SuppressWarnings("unused")
	private int maxReceivers;
	private int maxApChunks;
	//Constructors
	public NodeState(int id,int state, boolean source, int chunkCount) {
		super();
		maxChunks = Configuration.getInt("protocol.transport.maxchunks");
		maxApChunks = Configuration.getInt("protocol.applicative.maxApChunks");
		this.id = id;
		this.state = state;
		this.source = source;
		this.setChunks(new boolean[chunkCount]);
		this.setNextDest(-1);
		setDataRequested(false);
		this.state = NodeState.Idle;
		finished = false;
		activityTime = 0;
		inactivityTime= 0;
		lastActiveTime=0;
		lastInctiveTime=0;
	}

	public NodeState() {
		this(0,0,false,0);
	}

	// Getters and setters
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getState() {
		return state;
	}
	public void setState(int newState) {

		int previousState = this.state;
		this.state = newState;
		// The node becomes active
		if ( previousState == NodeState.Idle && newState != NodeState.Idle) {

			NetworkState.idleNodes -=1;
			NetworkState.activeNodes +=1;


			lastActiveTime = CommonState.getTime();

		} else if (previousState != NodeState.Idle && newState == NodeState.Idle){
			// The node becomes unactive
			System.out.println("NODACT:"+getId()+":"+lastActiveTime+":"+ CommonState.getTime());
			activityTime+=  CommonState.getTime() - lastActiveTime;

			NetworkState.idleNodes +=1;
			NetworkState.activeNodes -=1;


		}

	}
	public boolean isSource() {
		return source;
	}
	public void setSource(boolean isSource) {
		this.source = isSource;
		if(isSource==true){
			if (isFinished()) return;
			else {
				//setFinished(true);
				NetworkState.getInstance().nodeFinished(id);
			}
		}
	}
	private String stateToString() {

		switch (state) {

			case NodeState.Idle:
				return "Idle";

			case Downloading:
				return "Downloading";

			case Sending:
				return "Sending";

			case WaitingForData:
				return "WaitingForData";


			case Sleeping:
				return "Sleeping";

			case Exchanging:
				return "Exchanging";

			default:
				return "N.A";
		}
	}
	@Override
	public String toString(){
		return id +"\t"+getGroupId()+"\t"+this.stateToString()+"\t"+source+"\t"+getAvailableChunks()+"\n";
	}

	public boolean[] getChunks() {
		return chunks;
	}

	public void setChunks(boolean chunks[]) {
		this.chunks = chunks;
	}

	public void initChunks(int size) {
		this.chunks = new boolean[size];
	}
	// set the value of a chunk to true
	public void addChunk(int pos) {
		if (pos>=chunks.length) {
			return;
		}

		int tab[] = NetworkState.getInstance().chunksCpt;
		System.err.print("ChunksCpt = { ");
		for (int i:tab)
			System.err.print(i+" ");
		System.err.println("}");
		//NetworkState.getInstance().chunksCpt[pos]+=1;

		System.err.println("len="+NetworkState.getInstance().chunksCpt.length);
		chunks[pos]=true;
	}
	// Sets the value of a chunk to false
	public void removeChunk(int pos) {
		if (pos>=chunks.length) {
			return;
		}
		chunks[pos]=false;
	}

	public int getNodeType() {
		return nodeType;
	}

	public void setNodeType(int nodeType) {
		this.nodeType = nodeType;
	}

	public boolean isDataRequested() {
		return dataRequested;
	}

	public void setDataRequested(boolean dataRequested) {
		this.dataRequested = dataRequested;
	}

	public int getNextDest() {
		return nextDest;
	}

	public void setNextDest(int nextDest) {
		this.nextDest = nextDest;
	}
	// Get the indexes of the chunks that are available on the nodes
	public ArrayList<Integer> getAvailableChunks() {
		ArrayList<Integer> list = new ArrayList<Integer>();
		for ( int i=0 ; i<getChunks().length ; i++ ) {
			if (chunks[i])
				list.add(i);
		}
		return list;
	}
	// Get the indexes of the chunks that are not available on the nodes
	public ArrayList<Integer> getUnavailableChunks() {
		ArrayList<Integer> list = new ArrayList<Integer>();
		for ( int i=0 ; i<getChunks().length ; i++ ) {
			if (!chunks[i])
				list.add(i);
		}
		return list;
	}

	public int[] getUnavailableChunksArray() {
		ArrayList<Integer> list = getUnavailableChunks();
		int[] res= new int[list.size()];
		for ( int i =0 ; i < list.size(); i++) {
			res[i] = list.get(i);
		}
		return res;
	}


	// Get the chunks to get from another node
	public ArrayList<Integer> getChunksToExchange(NodeState other) {
		ArrayList<Integer> res = new ArrayList<Integer>();
		ArrayList<Integer> sourceAvailableChunks,destUnavailableChunks;
		sourceAvailableChunks = other.getAvailableChunks();
		destUnavailableChunks = this.getUnavailableChunks();

		for (Integer i : destUnavailableChunks) {
			if (sourceAvailableChunks.contains(i) && res.size() < MaxExchange) {
				res.add(i);
			}
		}
		return res;
	}

	public boolean hasAllTheChunks() {
		if (finished) return true;
		return getAvailableChunks().size() == NetworkState.getInstance().getChunkCount();
	}

	public static int getMaxExchange() {
		return MaxExchange;
	}

	public static void setMaxExchange(int maxExchange) {
		MaxExchange = maxExchange;
	}

	public long getNextAvailability() {
		return nextAvailability;
	}

	public  void setNextAvailability(long nextAvailability) {
		this.nextAvailability = nextAvailability;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public boolean isSleep() {
		return sleep;
	}

	public void setSleep(boolean sleep) {
		this.sleep = sleep;
	}

	public int[] getNextSend() {
		return nextSend;
	}

	public void setNextSend(int[] nextSend) {
		this.nextSend = nextSend;
	}


	public int getChunkFromOther(NodeState other) {
		// Get the first chunk that i don't have
		ArrayList<Integer> pickFrom = new ArrayList<Integer>();
		for (Integer i:other.getAvailableChunks()) {
			if (!getAvailableChunks().contains(i)) {
				pickFrom.add(i);
			}
		}
		if (pickFrom.size() > 0) {
			Collections.shuffle(pickFrom);
			return pickFrom.get(0);
		}
		return -1;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public boolean isBigConsumer() {
		return bigConsumer;
	}

	public void setBigConsumer(boolean bigConsumer) {
		this.bigConsumer = bigConsumer;
	}

	/**
	 * Get the chunks that i can download from the access point
	 * @param other : the other node
	 * @return my node
	 */
	public int[] getChunksFromOther(NodeState other) {
		ArrayList<Integer> res = new ArrayList<Integer>();
		ArrayList<Integer> myAvailable = getAvailableChunks();

		for (Integer available:other.getAvailableChunks()) {
			if (!myAvailable.contains(available) && res.size() != maxChunks) {
				res.add(available);
			}
		}
		int[] tab = new int[res.size()];
		for (int i = 0 ; i < res.size() ; i++) tab[i] = res.get(i);
		return tab;
	}


	public int[] getChunksFromAP() {
		ArrayList<Integer> res = new ArrayList<Integer>();
		ArrayList<Integer> unavailbleChunks = getUnavailableChunks();

		for (Integer unAvailable:unavailbleChunks) {
			if (res.size() < maxApChunks) {
				res.add(unAvailable);
			}
		}
		int[] tab = new int[res.size()];
		for (int i = 0 ; i < res.size() ; i++) tab[i] = res.get(i);
		return tab;
	}


	public int[] getRandomChunksFromOther(NodeState other) {
		ArrayList<Integer> res = new ArrayList<Integer>();
		ArrayList<Integer> myAvailable = getAvailableChunks();
		ArrayList<Integer> othersAvailableChunks = other.getAvailableChunks();
		Collections.shuffle(othersAvailableChunks,CommonState.r);


		////Systemout.println("Set ==> "+othersAvailableChunks);
		Iterator<Integer> it = othersAvailableChunks.iterator();
		while (it.hasNext() && res.size() < maxChunks ) {
			int available =  it.next();
			if (!myAvailable.contains(available) ) {
				res.add(available);
			}
		}
		int[] tab = new int[res.size()];
		for (int i = 0 ; i < res.size() ; i++) tab[i] = res.get(i);
		return tab;
	}


	public int[] getRandomChunksFromAP() {
		ArrayList<Integer> res = new ArrayList<Integer>();
		ArrayList<Integer> unavailable = getUnavailableChunks();
		Collections.shuffle(unavailable,CommonState.r);

		Iterator<Integer> it = unavailable.iterator();
		////Systemout.println("Set ==> "+unavailable);
		while (it.hasNext() && res.size() < maxApChunks ) {
			int available =  it.next();
			res.add(available);
		}
		int[] tab = new int[res.size()];
		for (int i = 0 ; i < res.size() ; i++) tab[i] = res.get(i);
		return tab;
	}

	public int hamming(NodeState other) {
		int res = 0;
		for (int i = 0 ; i < chunks.length ; i++) {
			res = (chunks[i] != other.getChunks()[i]) ? res+1 : res ;
		}
		return  res;
	}


	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

}


