package fr.lip6.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.config.Configuration;

public class NetworkState {

	private ArrayList<NodeState> nodeStates;

	private int networkSize;
	private static NetworkState instance = null;
	private int chunkCount;
	private boolean initialized;
	private boolean apAvailable;
	private int sourcesCount;
	private long lastTime;
	//private long nextApAvailability;
	private int finishedNodes;
	public static final int maxChunks = 2;
	//private double maxNodesPercent = 1.0;
	public static int maxNodes;

	public static int activeNodes; // Active nodes (state = Exchanging or Downloading or Sending)
	public static int idleNodes; // Idle nodes
	private int strategy;
	// Chunks counts here
	public static int[] chunksCpt;
    private int maxApChunks;

	//
	private String[] strategies={"Random","Sequential","Rarest First"};
	private static Logger log = (Logger) LogManager.getLogger(NetworkState.class);
	private NetworkState() {
		super();
		this.chunkCount = Configuration.getInt("init.initializer.chunksize");
        this.maxApChunks = Configuration.getInt("protocol.applicative.maxApChunks");
		this.maxNodes = Configuration.getInt("protocol.applicative.maxpar");
		this.strategy = Configuration.getInt("protocol.applicative.strategy");
		setInitialized(false);
		if (nodeStates == null)
			nodeStates = new ArrayList<NodeState>();

	}

	public static NetworkState getInstance() {
		if (instance == null)
			instance = new NetworkState();
		return instance;
	}

	public void newSource() {
		this.sourcesCount++;
	}

	public int getSourcesCount() {
		return sourcesCount;
	}

	public void init(int networkSize) {
		finishedNodes = 0;
		this.setNetworkSize(networkSize);
		log.info("chunkCount ="+ chunkCount);

		chunksCpt = new int [chunkCount];
		for (int i = 0  ; i < chunkCount ; i++)
			chunksCpt[i] = 0;

		sourcesCount = 0;
		if (nodeStates.size() > 0)
			nodeStates.clear();
		for (int i = 0; i < networkSize; i++) {
			NodeState ns = new NodeState(i, NodeState.Idle, false, chunkCount);
			ns.setState(NodeState.Idle);
			ns.setSource(false);
			nodeStates.add(ns);

		}
		idleNodes = networkSize;
		activeNodes = 0;
		setInitialized(true);
	}

	public NodeState getNodeState(int nodeID) {
		return nodeStates.get(nodeID);
	}

	public int getNetworkSize() {
		return networkSize;
	}

	public void setNetworkSize(int networkSize) {
		this.networkSize = networkSize;
	}

	public void changeState(long nodeId, int state, boolean source) {
		// Changer l'état de la case corresponsant aux noeud cherchant a accéder
		// à une donnée.
		this.nodeStates.get((int) nodeId).setSource(source);
		this.nodeStates.get((int) nodeId).setState(state);
	}

	public int getState(int nodeId) {
		// Changer l'état de la case corresponsant aux noeud cherchant a accéder
		// à une donnée.
		return nodeStates.get(nodeId).getState();
	}

	@Override
	public String toString() {
		String res = "\n------\n";
		for (NodeState ns : nodeStates) {
			res += ns;
		}
		res += "------\n";
		return res;

	}

	public boolean hasMoreChunksThanHalf(int nodeId) {

		int myChunks = getNodeState(nodeId).getAvailableChunks().size();
		int count = 0;
		for (NodeState ns : nodeStates) {
			if (nodeId != ns.getId()) {
				if (myChunks > ns.getAvailableChunks().size())
					count++;
			}
		}
		return count >= (nodeStates.size() / 2);
	}

	/**
	 * Get the id of an available ressource
	 * 
	 * @return -1 if no source has been found or the id of the node which
	 *         requested the data This function compares the chunks that my
	 *         node's content with the other nodes of the network
	 */
	public int getAvailableSource(int forDest) {
		/*
		 * The state controller looks at the network table For all the nodes it
		 * establishes the one my node will exchange with based on the diff of
		 * chunks between them
		 */
		ArrayList<NodeState> idleNodes = new ArrayList<NodeState>();

		for (NodeState ns : nodeStates) {
			if (ns.getState() == NodeState.Idle && ns.getId() != forDest )
				idleNodes.add(ns);
		}
		// If all the nodes are busy for the moment
		if (idleNodes.isEmpty())
			return -1;
		// Look for the node with the best score
		// Download a maximum of N nodes
		int selectedID =  - 1 ;
		int score = -1;
		int maxScore = 0;
		NodeState destState = this.nodeStates.get(forDest);
		for (NodeState sourceState : idleNodes) {
			score = 0;
			/*
			 * The score is computed this way: - We compare the chunk table. -
			 * +1 for each chunk that the nod has and that i don't have
			 */
			if (sourceState.getId() != forDest) {
				ArrayList<Integer> sourceAvailableChunks, destUnavailableChunks;
				sourceAvailableChunks = sourceState.getAvailableChunks();
				destUnavailableChunks = destState.getUnavailableChunks();
				for (Integer i : destUnavailableChunks) {
					if (sourceAvailableChunks.contains(i)) {
						score++;
					}
				}
				// Get the best node based on the choice criterion
				// Which is the node that has the most of the chunks that I
				// don't have
				if (score > maxScore) {
					selectedID = sourceState.getId();
					maxScore = score;
				}
			}
		}
		if (maxScore == 0) return -1;
		return selectedID;
	}

	public int getChunkCount() {
		return chunkCount;
	}

	public void setChunkCount(int chunkCount) {
		log.debug("ChunkCount = "+ chunkCount);
		this.chunkCount = chunkCount;

		for (NodeState ns : nodeStates) {
			boolean[] chunks = new boolean[this.chunkCount];
			ns.setChunks(chunks);
			for (int i = 0; i < this.chunkCount; i++) {
				ns.removeChunk(i);
			}
		}
	}

	public boolean isInitialized() {
		return initialized;
	}

	public void setInitialized(boolean initialized) {
		this.initialized = initialized;
	}

	public GiveTakeSet computeScore(int id) {
		GiveTakeSet res = new GiveTakeSet();
		NodeState ns = getNodeState(id);
		ArrayList<NodeState> availableNodes = new ArrayList<NodeState>();
		// Get the available nodes which are neither uploading, nor downloading

		for (NodeState n : nodeStates) {

			if (ns.getId() != n.getId()) {
				
				if (n.getState() == NodeState.Idle) {
					availableNodes.add(n);
				}
			}
		}
		//System.err.println(availableNodes);
		ArrayList<GiveTakeSet> scoresToCompute = new ArrayList<GiveTakeSet>();
		// For each node, compute the values i can offer and the values i can
		// get
		if (availableNodes.size() > 0) {
			ArrayList<Integer> mychunks = ns.getAvailableChunks();
			for (NodeState n : availableNodes) {
				if (ns.getId() != n.getId()) {
					int give = 0, take = 0;
					////System.err.println("Node  "+n);
					ArrayList<Integer> othersChunks = n.getAvailableChunks();
					// Compute how much chunks i can offer
					for (Integer i : mychunks) {
						if (!othersChunks.contains(i)) {
							give++;
						}
					}

					for (Integer i : othersChunks) {
						if (!mychunks.contains(i)) {
							take++;
						}
					}
					
					if (give == 0 && take == 0)
						continue;

					GiveTakeSet score = new GiveTakeSet(n.getId(), give, take);
					scoresToCompute.add(score);
				}

				if (scoresToCompute.size() > 0) {
					Collections.sort(scoresToCompute, new MyComparator());

					res = scoresToCompute.get(0);
					//System.err.println("====="+scoresToCompute+"\n==");
					return res;

				}
			}
		}

		return res;

	}

	private class MyComparator implements Comparator<GiveTakeSet> {
		//@Override
		public int compare(GiveTakeSet o1, GiveTakeSet o2) {
			return o1.compareTo(o2);
		}

	}


	public boolean apAvailable() {
		return apAvailable;
	}

	public void setApAvailable(boolean apAvailable) {
		this.apAvailable = apAvailable;
	}

	public ArrayList<NodeState> getNodeStates() {
		return nodeStates;
	}

	public void nodeFinished(int nodeId) {


        if (!nodeStates.get(nodeId).isFinished()) {

			nodeStates.get(nodeId).setFinished(true);
			finishedNodes++;
		}

	}

	public boolean finished() {
		/*
		 * boolean res = true; for (NodeState ns: nodeStates) res = res &&
		 * ns.isFinished(); return res;
		 */

		return finishedNodes == nodeStates.size();
	}

	public long getLastTime() {
		return lastTime;
	}

	public void setLastTime(long lastTime) {
		this.lastTime = lastTime;
	}

    /*Get the rarest chunk index*/
    public int getRarestChunk(){
        ArrayList<ChunkPair> chunkPairs= new ArrayList<ChunkPair>();
        for (int i = 0 ; i < chunksCpt.length ; i++) {
            chunkPairs.add(new ChunkPair(i,chunksCpt[i]));
        }
        log.info("ChunkPairs = "+ chunkPairs);
        Collections.sort(chunkPairs);
        return chunkPairs.get(0).index;
    }

	public int[]  getNRarestChunks(int n) throws ArrayIndexOutOfBoundsException {

		if (n > chunksCpt.length || n <= 0) {
			throw new ArrayIndexOutOfBoundsException("ChunksCpt has only"+chunksCpt.length+" elements ,not "+n);
		}

        ArrayList<ChunkPair> chunkPairs= new ArrayList<ChunkPair>();
        for (int i = 0 ; i < chunksCpt.length ; i++) {
            chunkPairs.add(new ChunkPair(i,chunksCpt[i]));
        }
        log.info("ChunkPairs = "+ chunkPairs);
        Collections.sort(chunkPairs);


        int [] res = new int[n];
        for (int i = 0 ; i < n ; i++) {
            res[i] = chunkPairs.get(i).index;
        }
		return res;
	}


	public int getFirstUncompletedNode(){
		for (NodeState n:nodeStates){
			if (n.getId() > 0 && n.getAvailableChunks().size() != n.getChunks().length && n.getState()==NodeState.Idle) {
				return n.getId();
			}
		}
		return  -1;
	}

	public int getRandomUncompletedNode(){
		ArrayList<Integer> ids = new ArrayList<Integer>();
		for (NodeState n:nodeStates){
			if (n.getId() > 0 && n.getAvailableChunks().size() != n.getChunks().length && n.getState()==NodeState.Idle) {
				ids.add(n.getId());
			}
		}
        Collections.shuffle(ids);
        if (ids.size()==0) return -1;
        return  ids.get(0);
	}



	public HammingDistance selectPair() {

		ArrayList<NodeState> list = new ArrayList<NodeState>();
		for (NodeState ns: nodeStates)
			if (ns.getId() > 0 && ns.getState() == NodeState.Idle  )
				list.add(ns);
		ArrayList<HammingDistance> distances = new ArrayList<HammingDistance>();

		for (NodeState ns:list)
			for (NodeState n2:list)
				if (ns != n2)
					distances.add(new HammingDistance(ns,n2));

		Collections.sort(distances);
        log.info("Distances"+distances);
        if (distances.size()==0 ||  distances.get( distances.size() - 1 ).getScore()==0)
            return null;

		return  distances.get( distances.size() - 1 );
	}
    /*Get the rarest chunk index*/

    public int[] getNRarestChunksForNode(NodeState node){

        ArrayList<ChunkPair> chunkPairs= new ArrayList<ChunkPair>();
        ArrayList<ChunkPair> chunkPairs2= new ArrayList<ChunkPair>();
        for (int i = 0 ; i < chunksCpt.length ; i++) {
            chunkPairs2.add(new ChunkPair(i,chunksCpt[i]));
        }
        Collections.sort(chunkPairs2);
        for (int i = 0 ; i < chunksCpt.length ; i++) {
            if (node.getUnavailableChunks().contains(i))
                chunkPairs.add(new ChunkPair(i,chunksCpt[i]));
        }
        Collections.sort(chunkPairs);
        log.info("ChunkPairs = "+ chunkPairs);
        if (chunkPairs.size()==0) {
            return null;
        }

        int tabsize = (chunkPairs.size() > maxApChunks )? maxApChunks : chunkPairs.size();
        int [] res = new int[tabsize];
        for ( int i = 0 ; i < tabsize ; i++ ) {
            res[i] = chunkPairs.get(i).index;
        }

        return res;
    }



}

