package fr.lip6.util;

public class GiveTakeSet implements Comparable<GiveTakeSet> {
	// The score computation will return three results ==> Wait, Sleep,download, upload
	public final static int DOWNLOAD = 0; // Downlaod
	public final static int UPLOAD = 1; // Upload 
	public final static int EXCHANGE = 2;// Exchange
	public final static int WAIT = 3; // There is 
	public final static int SLEEP = 4; //
	// The parameters of the computation
	private int give;
	private int take;
	private int id;
	private int result;
	
	
	public GiveTakeSet(int id,int give, int take) {
		this();
		this.id  = id;
		this.give = give;
		this.take = take;
	}
	
	public GiveTakeSet() {
		super();
		this.give =0;
		this.take =0;
		this.result = -1;
		
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GiveTakeSet other = (GiveTakeSet) obj;
		if (give != other.give)
			return false;
		if (take != other.take)
			return false;
		return true;
	}

	public int getGive() {
		return give;
	}
	public void setGive(int give) {
		this.give = give;
	}
	public int getTake() {
		return take;
	}
	public void setTake(int take) {
		this.take = take;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public int getResult() {
		return this.result;
	}
	
	public void setResult(int result) {
		this.result = result;
	}

	// Trier dans l'ordre pour faire remonter les éléments les plus importants en haut de liste

	public int compareTo(GiveTakeSet o) {
		return (getGive() + getTake()) - (o.getGive()+ o.getTake());
	}
	
	
	@Override
	public String toString() {
		String res = ""+id+ "\t:Give = "+getGive()+", Take = "+getTake()+ ", Res = ";
		if (give==0 && take==0) {
			
			res+="SLEEP";
		
		} else if (give==0 && take!=0) {
			
			res+="DOWNLOAD";
			
		} else if (give!=0 && take==0) {
			
			res+="UPLOAD";
		
		}
		else {
			res+="EXCHANGE";
		}
		
		return res;
	}
	
	public int getChoice() {
		if ( take == 0 ) {
			return SLEEP;
		} else if (give==0 && take!=0) {
			return DOWNLOAD;
		}
		return EXCHANGE;// Should be exchange
	}

	public int getChoice2() {
		if (give==0 && take==0) {
			return SLEEP;
		} else if (give==0 && take!=0) {
			return DOWNLOAD;
		} else if (give!=0 && take==0) {
			return SLEEP;
		}
		return EXCHANGE;// Should be exchange
	}
}

