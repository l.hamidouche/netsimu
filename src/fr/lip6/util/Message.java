package fr.lip6.util;


public class Message {
	

	// Message pour démarrer le fonctionnement
	public final static int START = 0;

	// Message pour attaquer la recherche d'un noeud
	public final static int TRY = 1;

	// Message pour lancer la recherche d'une source
	public final static int DOWNLOAD = 2;

	// Message pour demande
	public final static int GET = 3; // For a future use

	// Message pour Envoi
	public final static int PUT = 4; // For a future use

	// Message pour envoyer un message a un destinataire en attente d'une
	// donnée.
	public final static int SLEEP = 5;// Not implemented yet

	// Message pour réveiller le noeud
	public final static int WAKEUP = 6;
	
	// Message pour notifier la fin d'un téléchargemet 
	public final static int DOWNLOAD_ACK = 7;

	// Message pour enclanhcher un échange entre deux noeuds
	public final static int EXCHANGE = 8;

	// Message pour notifier la fin d'un échange
	public final static int FINISHED = 9;

	// Bouléen définissant le déclenchement de la simulation
	private boolean init ;


	// Types de messages
	public final static int[] message_types = {START,TRY,DOWNLOAD,GET,PUT,SLEEP,WAKEUP,DOWNLOAD_ACK,EXCHANGE,FINISHED};
	public final static String[] message_types_strings = {"START","TRY","DOWNLOAD","GET","PUT","SLEEP","WAKEUP","DOWNLOAD_ACK","EXCHANGE","FINISHED"};
	
	// Type de message
	private int type;

	private long source;

	private long destination;
	
	// Contenu du message : pourrait ne pas être utilisé
	private int content[];

	private int exchange_chunks[];

	// Echange ou pas
	private boolean exchange;

	//
	public Message(int type, int[] content, long source, long dest) {
		this.type = type;
		this.content = content;
		this.source = source;
		this.destination = dest;
		this.exchange = false;
	}

	public void setContent(int[] content) {

		this.content = content;
	}

	public int[] getContent() {
		return this.content;
	}

	// Content Size ===> number of chunks in the file
	public int getContentSize() {
		if (this.content != null)
			return content.length;
		return 0;
	}

	public int getType() {
		return this.type;
	}

	public long getSource() {
		return source;
	}

	public void setSource(long source) {
		this.source = source;
	}

	public boolean isExchange() {
		return exchange;
	}

	public void setExchange(boolean exchange) {
		this.exchange = exchange;
	}

	public String toString() {
		String res =  source + ":"+message_types_strings[getType()]+" ";
	

		if (content != null)
			for (int i = 0; i < content.length; i++)
				if (i== (content.length - 1)) res += content[i] + " ";
				else res += content[i] + ",";

		res += ":"+destination;
		return res;
	}

	public long getDestination() {
		return destination;
	}

	public void setDestination(long destination) {
		this.destination = destination;
	}


	public int[] getExchange_chunks() {
		return exchange_chunks;
	}

	public void setExchange_chunks(int[] exchange_chunks) {
		this.exchange_chunks = exchange_chunks;
	}

	public boolean isInit() {
		return init;
	}

	public void setInit(boolean init) {
		this.init = init;
	}

}
