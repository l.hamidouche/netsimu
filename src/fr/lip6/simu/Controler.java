package fr.lip6.simu;

import fr.lip6.util.NetworkState;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import peersim.core.CommonState;
import peersim.core.Control;
import peersim.core.Network;

public class Controler implements Control {
	private NetworkState networkState = NetworkState.getInstance();
	private static final Logger log = LogManager
			.getLogger();

	/**
	 * No specific parameter to initialize
	 * @param prefix
     */
	public Controler(String prefix) {

	}

	/*private String printTab(int []tab){
		String res = "{ ";
		for (int i = 0 ; i < tab.length; i++) {
			res+=tab[i]+" ";
		}
		res+="}";
		return res;
	}*/

	/**
	 * execute
	 * @return a value without any importance ==> current behaviour ==> output stats when simulaiton is done
     */
	public boolean execute() {
		if (networkState.finished()){

			for (int i = 0; i < Network.size() ; i++) {
				ApplicationNode app= (ApplicationNode) Network.get(i).getProtocol(ApplicationNode.mypid);
				app.printStats();
			}

			System.out.println("Finished at: "+ CommonState.getTime());
			System.exit(0);
		}

        System.out.println("ACTIVITY:"+ CommonState.getTime()+":"+networkState.activeNodes+":"+NetworkState.idleNodes);

		return false;
	}
}
