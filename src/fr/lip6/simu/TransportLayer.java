package fr.lip6.simu;

import fr.lip6.util.Message;
import fr.lip6.util.NetworkState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import peersim.config.*;
import peersim.core.*;
import peersim.edsim.*;

public class TransportLayer implements Protocol {

	// Fichier contenant la matrice de latences
	private long apDownloadTime;
	private long nodeDownloadTime;
	//private long timetowait;
	private long timeDelta;
	private long timeToSleep;
	private Logger log = LogManager
			.getLogger(TransportLayer.class.getName());

	/**
	 * Constructor: initializes the transport layer
	 * @param prefix
     */
	public TransportLayer(String prefix) {
		log.info("Transport Layer initialized");
		apDownloadTime = Configuration.getLong(prefix + ".aptime");
		nodeDownloadTime = Configuration.getLong(prefix + ".nodetime");
		timeToSleep = Configuration.getLong(prefix + ".timetosleep");
		timeDelta = Configuration.getLong(prefix + ".connectiondelta");
	}

	public Object clone() {
		return this;
	}

	public void send(Node src, Node dest, Object msg, int pid) {
		/* Get the delay relying on the type to connect to the node
		 Calculate the chunk count*/

		ApplicationNode s = (ApplicationNode) src
				.getProtocol(Configuration
						.getPid("init.initializer.protocolPid"));
		ApplicationNode d = (ApplicationNode) dest
				.getProtocol(Configuration
						.getPid("init.initializer.protocolPid"));

		Message m = (Message) msg;
		switch (m.getType()) {

			case Message.DOWNLOAD:
				long delay = getLatency(src, dest);
				int chunksCount = m.getContent().length;
				float f = (float) chunksCount
						/ (float) NetworkState.getInstance().getChunkCount();
				long time = (long) (delay * f);
				if (m.isExchange()) {
					time *= 1.3;
				}

				if (m.getSource() != 0) time += timeDelta + CommonState.r.nextPoisson(3.0) * 1000;

				//System.out.println("Time to transfer"+ msg+" from "+s.nodeId+"to "+d.nodeId+":"+time);
				EDSimulator.add(time, msg, dest, pid);
				break;


			case Message.START:
				if (!m.isInit())
					EDSimulator.add(1, msg, dest, pid);
				else
					EDSimulator.add(((Message) msg).getDestination() * 5, msg, dest, pid);

				break;

			case Message.TRY:
				EDSimulator.add(1, msg, dest, pid);
				break;

			case Message.GET:
				EDSimulator.add(1, msg, dest, pid);
				break;

			case Message.PUT:
				EDSimulator.add(2, msg, dest, pid);
				break;

			case Message.SLEEP:
				EDSimulator.add(1, msg, dest, pid);
				break;

			case Message.WAKEUP:
				EDSimulator.add(timeToSleep, msg, dest, pid);
				break;

			case Message.EXCHANGE:
				EDSimulator.add(1, msg, dest, pid);
				break;
			default:
				EDSimulator.add(1, msg, dest, pid);
				break;
		}

	}

	/**
	 * @function getLatency: computes the latency between two devices
	 * @param src the source
	 * @param dest the destination
     * @return
     */
	public long getLatency(Node src, Node dest) {

		ApplicationNode source = (ApplicationNode) src
				.getProtocol(Configuration
						.getPid("init.initializer.protocolPid"));

		if (source.getNodeId() == 0) {

			return apDownloadTime;

		} else {

			return nodeDownloadTime;
		}

	}

}
