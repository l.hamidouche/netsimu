package fr.lip6.simu;

import fr.lip6.util.Message;
import fr.lip6.util.NetworkState;
import fr.lip6.util.NodeState;
import peersim.core.*;
import peersim.config.*;


public class Initializer implements peersim.core.Control {

	private int ApplicationNodePid;


	private NetworkState networkState;

	private int chunkSize;

	public Initializer(String prefix) {

		/**
		 * Protocol layer
		 */
		this.ApplicationNodePid = Configuration.getPid(prefix + ".protocolPid");
		this.networkState = NetworkState.getInstance();
		this.chunkSize = Configuration.getInt(prefix + ".chunksize");
		// Initialization
		networkState.init(Network.size());
		networkState.setChunkCount(chunkSize);
		networkState.setApAvailable(true);
		
	}

	public boolean execute() {

		int nodeNb;
		ApplicationNode current;
		Node dest;
		// recuperation de la taille du reseau
		nodeNb = Network.size();
		// creation du message
		if (nodeNb < 1) {
			System.err.println("Network size is not positive");
			System.exit(1);
		}
		System.err.println("Number of nodes "+nodeNb);
		// Node initialization without initializing states
		for (int i = 0; i < nodeNb ; i++) {
			// Get the node i
			dest = Network.get(i);
			current = (ApplicationNode) dest
					.getProtocol(this.ApplicationNodePid);
			current.setTransportLayer(i);
			//current.setNodeType(ApplicationNode2.NODE);
			networkState.getNodeState(i).setSleep(false);;
			// Initialize chunk tables =====> node 0 is the access point
			for (int c = 0; c < chunkSize; c++) {
				networkState.getNodeState(i).getChunks()[c] = (i==0);
			}

			NetworkState.getInstance().changeState(i, NodeState.Idle, i==0);
			int[] tab = {};
			if (i==0){
				// Start the access pointgit 
				Message mess = new Message(Message.START, tab, i,i);
				mess.setInit(true);
				Node src = Network.get(0);
				ApplicationNode srcApp = (ApplicationNode) src
						.getProtocol(ApplicationNodePid);
				srcApp.send(mess, dest);
			}
			//System.out.println(networkState);
		}
		if (nodeNb < 2) {
			System.err
					.println("At least 2 nodes are needed in the ring version");
			System.exit(1);
		}

		System.err.println("Initialization completed");
		return false;
	}

}
