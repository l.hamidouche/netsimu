package fr.lip6.simu;

import java.util.HashMap;


import fr.lip6.util.HammingDistance;
import fr.lip6.util.NodeState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Network;
import peersim.core.Node;
import peersim.edsim.EDProtocol;
import fr.lip6.util.Message;
import fr.lip6.util.NetworkState;

import static fr.lip6.util.NodeState.*;

public class ApplicationNode implements EDProtocol {

	// identifiant de la couche transport
	protected int transportPid;

	// objet couche transport
	protected TransportLayer transport;

	// identifiant de la couche courante (la couche applicative)
	public static int mypid;

	// le numero de noeud
	protected int nodeId;

	// prefixe de la couche (nom de la variable de protocole du fichier de
	// config)
	protected String prefix;

	// temps d'attente moyen avant de télécharger une donnée
	protected int timetowait;

	// The data structure
	protected NetworkState networkState;

	HashMap<Integer, Integer> sentStats;
	HashMap<Integer, Integer> recvStats;

    int chunksFromNodes;
    int chunksFromAP;
    private int[] emptyMessage= {};
	private int currentExchange;
	private boolean exchangeACK;

	protected long lastSleep;
	protected long totalSleep;
	protected long lastReceived;

	protected int sent;
	protected int received;


    protected  int maxTransferts;
    protected  int nbActive;
	private static final Logger log = LogManager
			.getLogger(ApplicationNode.class.getName());

	//
	private int strategy;
	private int maxApChunks;
	// Constructor
	public ApplicationNode(String prefix) {
        this.chunksFromAP=0;
        this.chunksFromNodes = 0;
		this.maxApChunks = Configuration.getInt(prefix+".maxApChunks");
        this.maxTransferts = Configuration.getInt(prefix+".maxpar");
        this.nbActive = 0;
		this.prefix = prefix;
		this.transportPid = Configuration.getPid(prefix + ".transport");
		this.mypid = Configuration.getPid(prefix + ".myself");
		this.strategy = Configuration.getInt(prefix+".strategy");
		sent = 0;
		received = 0;
		this.transport = null;
		this.networkState = NetworkState.getInstance();
		this.lastSleep = 0;
		this.totalSleep = 0;
		initMessageStats();
	}

	protected void initMessageStats() {
		this.sentStats = new HashMap<Integer, Integer>();
		this.recvStats = new HashMap<Integer, Integer>();

		for (int i : Message.message_types) {
			sentStats.put(i, 0);
			recvStats.put(i, 0);
		}

	}

	// methode appelee lorsqu'un message est recu par le protocole
	// ApplicationNode du noeud
	// Faire les changements sur NetworkState
	public void processEvent(Node node, int pid, Object event) {
		Message m = (Message) event;
		// Message reception
		recvStats.put(m.getType(), recvStats.get(m.getType()) + 1);
		setLastReceived(CommonState.getTime());
		this.receive(m);
	}

	// methode necessaire pour la creation du reseau (qui se fait par clonage
	// d'un prototype)
	public Object clone() {
		ApplicationNode dolly = new ApplicationNode(this.prefix);
		return dolly;
	}

	// liaison entre un objet de la couche applicative et un
	// objet de la couche transport situes sur le meme noeud
	public void setTransportLayer(int nodeId) {
		this.nodeId = nodeId;
		this.transport = (TransportLayer) Network.get(this.nodeId).getProtocol(
				this.transportPid);
	}

	// envoi d'un message (l'envoi se fait via la couche transport)
	public void send(Message msg, Node dest) {
		sent++;
		sentStats.put(msg.getType(), sentStats.get(msg.getType()) + 1);
		this.transport.send(getMyNode(), dest, msg, this.mypid);
	}


	protected void receive(Message msg) {
		received++;
        lastReceived = CommonState.getTime();
		NodeState mystate = networkState.getNodeState(nodeId);
		NodeState other = networkState.getNodeState((int)msg.getSource());

        System.out.println(CommonState.getTime() + ":" + msg);

        if (networkState.finished()){
            ApplicationNode tmp = null;
            for (int i = 0; i < Network.size() ; i++) {
                ApplicationNode app= (ApplicationNode) Network.get(i).getProtocol(ApplicationNode.mypid);
                app.printStats();
                if (i==0) {
                    tmp = app;

                }

            }
            System.out.println("APNOD:"+tmp.chunksFromNodes+":"+tmp.chunksFromAP);
            System.out.println("Finished at: "+ CommonState.getTime());
            System.exit(0);
        }

        if (nodeId == 0) {


            //  System.out.println("ACTIVITY:"+ CommonState.getTime()+":"+networkState.activeNodes+":"+NetworkState.idleNodes);
            // Depenging on  mobile nodes messages
            switch (msg.getType()) {
                case Message.FINISHED:
                    // Add content to my local chunks
                    // Send receive notification to ap
                    NodeState source = networkState.getNodeState((int)msg.getSource());
                    NodeState destination = networkState.getNodeState((int)msg.getDestination());

                    if (source.getState() == NodeState.Downloading) {

                        // The chunk has been downloaded from the access point
                        source.setState(NodeState.Idle);
                        destination.setState(NodeState.Idle);

                    }
                    if (source.getState()== NodeState.Sending ) {

                        source.setState(NodeState.Idle);
                        destination.setState(NodeState.Idle);

                    } else if (source.getState() == NodeState.Exchanging) {

                        destination.setState(Downloading);
                        source.setState(Sending);

                    }
                    break;
            }

            ///System.out.println(networkState);
            // Access point case
            if (mystate.getState() == NodeState.Idle && !networkState.finished()) {
                int node = networkState.getRandomUncompletedNode();
                if (node == -1) {
                    // Reschedule a wake up event soon
                    //return;
                }  else {
                    NodeState nodeSt = networkState.getNodeState(node);
                    //int chunks[] = nodeSt.getRandomChunksFromAP();

                    int chunks[] = networkState.getNRarestChunksForNode(nodeSt);
                    chunksFromAP+=chunks.length;
                    if (chunks!= null) {

                        System.err.print("---");
                        for (int chunk : chunks) {
                            System.err.print(" " + chunk);
                        }
                        System.err.print("\n");

                        for (int pos: chunks)
                            NetworkState.getInstance().chunksCpt[pos]+=1;

                        Message m = new Message(Message.DOWNLOAD, chunks, 0, node);
                        send(m, Network.get(node));

                        nodeSt.setState(NodeState.Downloading);
                        mystate.setState(NodeState.Sending);
                    }

                }

            }
            HammingDistance pair = networkState.selectPair();
            while (networkState.activeNodes < maxTransferts && pair != null) {

                if (pair==null){
                    return;
                }
                int chunksNode1[] = pair.getNode1().getChunksFromOther(pair.getNode2());
                int chunksNode2[] = pair.getNode2().getChunksFromOther(pair.getNode1());
                for (int pos: chunksNode1)
                    NetworkState.getInstance().chunksCpt[pos]+=1;
                for (int pos: chunksNode2)
                    NetworkState.getInstance().chunksCpt[pos]+=1;
                chunksFromNodes+= chunksNode1.length+chunksNode2.length;
                if (chunksNode1.length > 0 && chunksNode2.length ==0) {

                    // Node 2 sends to node 1
                    Message toSend = new Message(Message.GET,chunksNode1,pair.getNode1().getId(),
                            pair.getNode2().getId());
                    send(toSend,Network.get(pair.getNode2().getId()));
                    pair.getNode1().setState(NodeState.Downloading);
                    pair.getNode2().setState(NodeState.Sending);

                } else if (chunksNode2.length > 0 && chunksNode1.length ==0) {

                    // Node 2 sends to node 1
                    Message toSend = new Message(Message.GET,chunksNode2,pair.getNode2().getId(),
                            pair.getNode1().getId());
                    send(toSend,Network.get(pair.getNode1().getId()));

                    pair.getNode2().setState(NodeState.Downloading);
                    pair.getNode1().setState(NodeState.Sending);

                } else {

                    // Exchange between the two nodes
                    Message toSend = new Message(Message.EXCHANGE,chunksNode1,pair.getNode1().getId(),
                            pair.getNode2().getId());
                    toSend.setExchange(true);
                    toSend.setExchange_chunks(chunksNode2);
                    send(toSend,Network.get(pair.getNode2().getId()));
                    pair.getNode1().setState(NodeState.Exchanging);
                    pair.getNode2().setState(NodeState.Exchanging);
                }
                pair = networkState.selectPair();
            }

            //
        } else {
            // Mobile node case
            switch (msg.getType()) {
                case Message.DOWNLOAD:
                    // Add content to my local chunks
                    if (mystate.getState() != NodeState.Downloading &&
                            mystate.getState() != NodeState.Exchanging ) {
                        log.error("The state of the node should be either Downloading or Exchanging");
                        System.exit(10);
                    }
                    for (int chunk : msg.getContent()){
                        mystate.addChunk(chunk);
                    }
                    if (mystate.getUnavailableChunks().size() ==0) {
                        mystate.setSource(true);
                    }
                    // Set both nodes to idle, send ACK to node and finished to AP
                    if (msg.getSource() == 0 ) {

                         if (mystate.getUnavailableChunks().size()==0){
                                mystate.setSource(true);
                            }
                            Message endMessage = new Message(Message.FINISHED,emptyMessage,nodeId,msg.getSource());
                            send(endMessage,Network.get((int)msg.getSource()));
                            // Send message Finished to ap
                            // Go back to Idle state
                    } else {
                        // Send ACK message to the node which sent the message
                        // The receiver of ack will send FINISHED MESSAGE TO the AP

                        if (mystate.getState() == NodeState.Downloading) {
                            Message endMessage = new Message(Message.DOWNLOAD_ACK, emptyMessage, nodeId, msg.getSource());
                            send(endMessage, Network.get((int) msg.getSource()));
                        }
                        if (mystate.getState() == NodeState.Exchanging) {
                            Message endMessage = new Message(Message.FINISHED,emptyMessage,nodeId,msg.getSource());
                            send(endMessage,Network.get(0));
                        }

                    }

                    // Send receive notification to ap
                    break;
                case Message.GET:
                    // make sure that i'm in download state and
                    // send Download message to the source of the message
                    if (mystate.getState() != NodeState.Sending && mystate.getState() != NodeState.Exchanging) {
                        log.error("Wrong state, the node's state shoud be either Sending("+
                                NodeState.Sending+")or Exchanging("+NodeState.Sending+"), but the state is  "+mystate.getState());
                        System.exit(20);
                    }
                    Message response = new Message(Message.DOWNLOAD,msg.getContent(),nodeId,msg.getSource());
                    send(response, Network.get((int)msg.getSource()));
                    break;
                case Message.EXCHANGE:
                    // send download message and get message and change states
                    // Send get message with the chunks to exchange
                    if ( mystate.getState() != NodeState.Exchanging ) {
                        log.error("Wrong state, the node's state shoud be either Sending("+
                                NodeState.Sending+")or Exchanging("+NodeState.Sending+"), but the state is  "+mystate.getState());
                        System.exit(20);
                    }

                    Message getMessage = new Message(Message.GET,msg.getExchange_chunks(),nodeId,msg.getSource());
                    send(getMessage, Network.get((int)msg.getSource()));
                    Message downloadMessage = new Message(Message.DOWNLOAD,msg.getContent(),nodeId,msg.getSource());
                    send(downloadMessage, Network.get((int)msg.getSource()));

                    break;


                case Message.DOWNLOAD_ACK:

                    if (mystate.getState()!=NodeState.Sending && mystate.getState() != NodeState.Exchanging) {
                        log.error("Only sending or exchanging nodes can receive this message");
                        System.exit(20);
                    }
                    if (mystate.getState() == NodeState.Sending) {
                        Message end = new Message(Message.FINISHED,emptyMessage,msg.getSource(),nodeId);
                        send(end,Network.get(0));
                    }

                    break;

                default:
                    log.error("What ? happpened here");
                    break;
            }

        }
        //System.out.println(networkState);
	}

	//<editor-fold desc="Getters and Setters">
	// retourne le noeud courant
	protected Node getMyNode() {
		return Network.get(this.nodeId);
	}

	public String toString() {
		return "Node " + this.nodeId;
	}

	public NetworkState getNetworkState() {
		return networkState;
	}

	public void setNetworkState(NetworkState networkState) {
		this.networkState = networkState;
	}

	public int getTimetowait() {
		return timetowait;
	}

	public void setTimetowait(int timetowait) {
		this.timetowait = timetowait;
	}

    public int getNodeId() {
        return nodeId;
    }

    public void setNodeId(int nodeId) {
        this.nodeId = nodeId;
    }

    public long getLastSleep() {
        return lastSleep;
    }

    public void setLastSleep(long lastSleep) {
        this.lastSleep = lastSleep;
    }

    public long getTotalSleep() {
        return totalSleep;
    }

    public void setTotalSleep(long totalSleep) {
        this.totalSleep = totalSleep;
    }

    public long getLastReceived() {
        return lastReceived;
    }

    public void setLastReceived(long lastReceived) {
        this.lastReceived = lastReceived;
    }


    public void printStats() {

        System.out.println("NODE_STA:" + nodeId + ":" + networkState.getNodeState(nodeId).getActivityTime() + ":"
                + (CommonState.getTime() - networkState.getNodeState(nodeId).getActivityTime()) + ":" + sent + ":" + received
                + ":" + this.getLastReceived());


        for (int i : Message.message_types) {
            System.out
                    .println("RECV_STA:" + nodeId + ":"
                            + Message.message_types_strings[i] + ":"
                            + recvStats.get(i));
        }

        for (int i : Message.message_types) {
            System.out
                    .println("SENT_STA:" + nodeId + ":"
                            + Message.message_types_strings[i] + ":"
                            + sentStats.get(i));
        }

    }

    //</editor-fold>

}