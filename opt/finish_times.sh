#! /bin/bash

CUR=`pwd`
if [ $# -ne 1 ] ; then
    echo 'Requested argument: directory'
    exit 0
fi

cd $1
echo "Network size,Chunk count, Connection delta, AP time,Node Time, Sleep Time, Connection delta, Maximum parallel nodes,Strategy, Maximum chunks between nodes, maximum access point chunks, Group size, Experiment,Completion Time(s)" > finish_times.csv



for i in *.out; do 
	start=`echo -n $i | sed 's/simu//' | sed 's/^-//' | sed 's/\.out//' |tr '-' ','`
	end=`grep "Finished at" $i | sed  's/Finished at: //'`
	if ! [[ -z $end ]] ; then
		echo  "$start,`expr $end / 1000`" >> finish_times.csv
	fi
done;

cd "$CUR"
