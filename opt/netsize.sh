#! /bin/bash
if [[ -d "netsize" ]]; then
    mv netsize back/netsize-`ls back/netsize* | wc -l`
fi
mkdir netsize/
mkdir netsize/out/
TIMETOWAIT=2000



STRATEGY=2
#network-size
for NETSIZE in 51 61 71 81 101 151 201 ; do
  # chunk size
  for CHUNKSIZE in 20 ; do
    #Policy
    for SIMU in "simu" ; do

      for APTIME in 10247 ; do

        for NODE_TIME in 122333 ; do

          for SLEEP_TIME in 1000; do

            for CONNECTION_DELTA in 4000 ; do

                for MAX_NODES in 1 33; do

                    for MAX_AP_CHUNKS in 1  ; do #

                       for MAX_CHUNKS in  1 ; do

                          for GROUP_SIZE in 30 ; do

			            for EXPERIMENT in 1 ; do


                                    APPLICATIVE="fr.lip6.$SIMU.ApplicationNode"
                                    TRANSPORT="fr.lip6.$SIMU.TransportLayer"
                                    INITIALIZER="fr.lip6.$SIMU.Initializer"
				                            SEED=$RANDOM # Returns a timestamp
                                    FILE="$SIMU-$NETSIZE-$CHUNKSIZE-$TIMETOWAIT-$APTIME-$NODE_TIME-$SLEEP_TIME-$CONNECTION_DELTA-$MAX_NODES-$STRATEGY-$MAX_CHUNKS-$MAX_AP_CHUNKS-$GROUP_SIZE-$EXPERIMENT"
                                    echo $FILE
                                    cat config.template.cfg | sed -e "s/__CHUNKS__/$CHUNKSIZE/" | sed -e "s/__NETSIZE__/$NETSIZE/" | sed -e "s/__TIMETOWAIT__/$TIMETOWAIT/" | sed -e "s/__GROUP_SIZE__/$GROUP_SIZE/"| sed -e "s/__AP_TIME__/$APTIME/" | sed -e "s/__NODE_TIME__/$NODE_TIME/" |sed -e "s/__SLEEP_TIME__/$SLEEP_TIME/" | sed -e "s/__CONNECTION_DELTA__/$CONNECTION_DELTA/" | sed -e "s/__SEED__/$SEED/" | sed -e "s/__APPLICATIVE__/$APPLICATIVE/" |sed -e "s/__INITIALIZER__/$INITIALIZER/" | sed -e "s/__TRANSPORT__/$TRANSPORT/" | sed -e "s/__STRATEGY__/$STRATEGY/" | sed -e "s/__MAX_AP_CHUNKS__/$MAX_AP_CHUNKS/" | sed -e "s/__MAX_NODES__/$MAX_NODES/" | sed -e "s/__MAX_CHUNKS__/$MAX_CHUNKS/" > netsize/$FILE.cfg

                                    java -classpath ../bin:../src:../lib/jep-2.3.0.jar:../lib/djep-1.0.0.jar:../lib/peersim-1.0.5.jar:../lib/log4j-api-2.5.jar:../lib/log4j-core-2.5.jar peersim/Simulator netsize/$FILE.cfg > netsize/$FILE.out
                                    #python analyse.py netsize/$FILE.out > netsize/out/$FILE.csv
                                done;
                            done;
                        done
                    done;
                done;
              done;
            done;
          done;
        done;
      done;
    done;
  done;
done;
