#!/bin/env/python
import sys
import os
import pandas as pd
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
#import seaborn as sb
import string
import argparse
from matplotlib.font_manager import FontProperties
import seaborn as sb

def read_csv(file, sep=";"):
    df = pd.read_csv(file,sep)
    return df
##
def plot_lines(df,output_file):
    columns = df.columns
    index = df[columns[0]]
    df.index = index
    df.index.name = columns[0]
    # Print legends under the plot
    plot = df.plot(kind='line',x=columns[0])
    plot.set_ylim([plot.get_ylim()[0],plot.get_ylim()[1]+5])
    plt.ylabel('Completed nodes')
    plt.xticks(rotation="horizontal")
    fig = plt.gcf()
    fig.set_size_inches(8 , 5)
    fig.savefig(output_file,dpi=300)
    return plot

def plot_lines_bis(df,output_file):
    columns = df.columns
    index = df[columns[0]]
    df.index = index
    df.index.name = columns[0]
    # Print legends under the plot
    plot = df.plot(kind='line',x=columns[0])
    plot.set_ylim([plot.get_ylim()[0],plot.get_ylim()[1]+5])
    plt.ylabel('Downloaded chunks')
    plt.xticks(rotation="horizontal")
    fig = plt.gcf()
    fig.set_size_inches(8 , 5)
    fig.savefig(output_file,dpi=300)
    return plot

def plot_lines2(df,output_file):
    columns = df.columns
    index = df[columns[0]]
    df.index = index
    df.index.name = columns[0]
    # Print legends under the plot
    plot = df.plot(kind='line',x=columns[0])
    plot.set_ylim([plot.get_ylim()[0],plot.get_ylim()[1]+5])
    plt.ylabel('Downloaded chunks')
    plt.xticks(rotation="horizontal")
    fig = plt.gcf()
    fig.set_size_inches(8 , 5)
    fig.savefig(output_file,dpi=300)
    return plot

def plot_lines3(df,output_file):
    columns = df.columns
    index = df[columns[0]]
    df.index = index
    df.index.name = columns[0]
    # Print legends under the plot
    plot = df.plot(kind='line',x=columns[0])
    plot.set_ylim([plot.get_ylim()[0],plot.get_ylim()[1]+5])
    plt.ylabel('Completion time(s)')
    plt.xticks(rotation="horizontal")
    fig = plt.gcf()
    fig.set_size_inches(8 , 5)
    fig.savefig(output_file,dpi=300)
    return plot


def plot_lines4(df,output_file):
    columns = df.columns
    index = df[columns[0]]
    df.index = index
    df.index.name = columns[0]
    # Print legends under the plot
    plot = df.plot(kind='line',x=columns[0],use_index=True,logx=True)
    #plot.set_xscale('log')
    plot.set_xticks(df[columns[0]])
    plot.set_xticklabels(df[columns[0]])
    # font = {'style' : 'normal','weight' : 'bold','size'   : 25}
    # plt.rc('font', **font)
    plot.set_ylim([plot.get_ylim()[0],plot.get_ylim()[1]+5])
    plt.ylabel('Completion time(s)')
    plt.xticks(rotation="horizontal")
    fig = plt.gcf()
    fig.set_size_inches(16 , 10)
    fig.savefig(output_file,dpi=300)
    return plot




def plot_bars(df,output_file):
    columns = df.columns
    index = df[columns[0]]
    df.index = index
    df.index.name = columns[0]
    # Print legends under the plot
    plot = df.plot(kind='bar',x=columns[0])
    plot.set_ylim([plot.get_ylim()[0],plot.get_ylim()[1]*1.2])
    plt.ylabel('Completion time(s)')
    plt.xticks(rotation="horizontal")
    fig = plt.gcf()
    fig.set_size_inches(8 , 5)
    fig.savefig(output_file,dpi=300)
    pass
def plot_bars2(df,output_file):
    columns = df.columns
    index = df[columns[0]]
    df.index = index
    df.index.name = columns[0]
    # Print legends under the plot
    plot = df.plot(kind='bar',x=columns[0])
    plot.set_ylim([plot.get_ylim()[0],plot.get_ylim()[1]*1.2])
    plt.ylabel('Completion time(s)')
    plt.xticks(rotation="horizontal")
    fig = plt.gcf()
    fig.set_size_inches(8 , 5)
    fig.savefig(output_file,dpi=300)
    pass

def plot_cummilative_lines(df,output_file):
    pass

df = read_csv("data_cp_chunks.csv")
plot_lines4(df,"plot2.png")
#
# df = read_csv("df_sleep_impact.csv")
# plot_bars(df,"sleep_impact.png")
#
df = read_csv("data_cp_time.csv")
plot_lines3(df,"plot3.png")
#
df = read_csv("df_comparison.csv")
plot_lines(df,"plot1.png")
#
df = read_csv("chunks.csv")
plot_lines2(df,"plot1-bis.png")

df = read_csv("wifi2.csv")
plot_lines3(df,"wifiPlot.png")
