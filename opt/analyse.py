#! /bin/env/python
#Data Structure
#
#<EMPTY LINE>*(4~6)
#TIMESTAMP:SENDER:MESSAGE_TYPE <CONTENT1 CONTENT2 ...> :RECEIVER
#TIMESTAMP:SENDER:MESSAGE_TYPE <CONTENT1 CONTENT2 ...> :RECEIVER
#TIMESTAMP:SENDER:MESSAGE_TYPE <CONTENT1 CONTENT2 ...> :RECEIVER
#...
#NODE_STA:NODE_ID:SLEEP_TIME:ACTIVE_TIME:SENT_MESSAGES:RECEIVED_MESSAGES:LAST_MESSAGE_RECEIVED
#RECV_STA:NODE_ID:MSG_TYPE:COUNT
#...
#
#SENT_STA:NODE_ID:MSG_TYPE:COUNT
#...
# Input: FILENAME
# Output: TBD
import sys
import os

if (len(sys.argv) != 3):
    print "Usage: "+sys.argv[0]+" <FILENAME> <OUTPUT_FOLDER>"
    exit(1);

filename = sys.argv[1]
dest = sys.argv[2]
#print 'Processing data for : ', filename
if ( not os.path.isfile(filename)):
    print "You must provide a file"
    exit(1)
myfile = None
try:
    myfile = open(filename, "r") # or "a+", whatever you need
except IOError:
    print "Could not open file!"
    exit(0)

##
# Structure of the events
#[TIMESTAMP,SENDER_ID,["MESSAGE_TYPE", CHUNK ],RECEIVER_ID]]
events=[]
## Structure of nodes stats
node_stats={}

message_stats={}
node_stats = {}
max_timestamp=0
end_time = 0
filepath=filename
filename = filepath.split(os.sep)[-1]
no_ext = os.path.splitext(os.path.basename(filename))[0]
chunk_size= int(filepath.split(os.sep)[-1].split('.')[0].split("-")[2])
network_size = int(filepath.split(os.sep)[-1].split('.')[0].split("-")[1])
#print chunk_size
## On verra
activity_logs=[]
activity_segs=[]
for line in myfile:
    l = line.replace("\n","")
    components = l.split(":");

    if len(components)  == 4:
        print components
        if l.startswith("SENT") or l.startswith("RECV"):
            node_id = int(components[1])
            try:
                if node_stats[node_id][components[1]] :
                    node_stats[node_id][components[1]][components[2]]=int(components[3])
            except KeyError as e:
                node_stats[node_id][components[1]]= {components[2]:int(components[3])}
        elif l.startswith("NODACT") :
            activity_segs.append(components[1::])
        elif l.startswith("ACTIVITY") :
            print "ACTIVITY"
            activity_logs.append(components[1::])
        else:
            # Store the events
            event=[]
            timestamp = long(components[0])
            if (end_time < timestamp):
                end_time = timestamp

            senderId = int(components[1])
            receiverId = int(components[3])
            message_content=components[2].split(" ")
            if (len(message_content) >2):
                ####
                event=[timestamp,senderId,[message_content[0],message_content[1].split(",")],receiverId]
                ####
            else:
                event=[timestamp,senderId,[message_content[0]],receiverId]
                # Global message stats
            events.append(event)
            try:
                if (message_stats[message_content[0]]):
                    message_stats[message_content[0]] = message_stats[message_content[0]] + 1
                else:
                    message_stats[message_content[0]] = 1
            except KeyError as e:
                message_stats[message_content[0]]=1


    if l.startswith("NODE"):
        node_id = int(components[1])
        #print components
        try:
            if (node_stats[node_id]):
                #NODE_STA:NODE_ID:SLEEP_TIME:ACTIVE_TIME:SENT_MESSAGES:RECEIVED_MESSAGES:LAST_MESSAGE_RECEIVED
                #RECV_STA:NODE_ID:MSG_TYPE:COUNT
                node_stats[node_id]["SLEEP_TIME"] = int(components[2])
                node_stats[node_id]["ACTIVE_TIME"] = int(components[3])
                node_stats[node_id]["SENT_MESSAGES"] = int(components[4])
                node_stats[node_id]["RECEIVED_MESSAGES"] = int(components[5])
                node_stats[node_id]["RECEIVED_MESSAGES"]  = int(components[6])
        except KeyError as e:
            node_stats[node_id]={}
            node_stats[node_id]["SLEEP_TIME"] = int(components[2])
            node_stats[node_id]["ACTIVE_TIME"] = int(components[3])
            node_stats[node_id]["SENT_MESSAGES"] = int(components[4])
            node_stats[node_id]["RECEIVED_MESSAGES"] = int(components[5])
            node_stats[node_id]["RECEIVED_MESSAGES"]  = int(components[6])

    if len(components)==2:
        if components[0].startswith("EndTime"):
            end_time = int(components[1])

# Dowload
messages = [0]*(int(end_time/1000)+1)
message_count = [0]*(int(end_time/1000)+1)
tab = [0]*(int(end_time/1000)+1)
tab2 = [0]*(int(end_time/1000)+1)
i=0

completed = [0]*(int(end_time/1000)+1)
completed_acc = [0]*(int(end_time/1000)+1)

for key in node_stats.keys():
    node_stats[key]["CHUNKS"]=0
    node_stats[key]["FINISHED"]= False
    node_stats[key]["ENDTIME"]= 0
    #if (keys[0]==)
for e in events:
    #print int(e[0]/1000) ," endtime=",(end_time/1000)

    if (e[2][0] == "DOWNLOAD"):
        tab[int(e[0]/1000)] = tab[int(e[0]/1000)] + len(e[2][1])
        #print node_stats.keys()
        node_stats[e[-1]]["CHUNKS"]= node_stats[e[-1]]["CHUNKS"] + len(e[2][1])
        #print node_stats[e[-1]]["CHUNKS"]
        if (not node_stats[e[-1]]["FINISHED"]):
            if (node_stats[e[-1]]["CHUNKS"] == chunk_size):
                node_stats[e[-1]]["FINISHED"] = True
                node_stats[key]["ENDTIME"]= e[0]
                completed[int(e[0]/1000)] = completed[int(e[0]/1000)]  + 1
    messages[int(e[0]/1000)] = messages[int(e[0]/1000)] + 1
f1 = open(dest+os.sep+no_ext+".csv", 'w')
f1.write('Time(s),Downloaded cunks,Total Downloaded chunks,Messages count,Total messages count,Completed,Total completed\n')
#Write block evolution
#print 'Timestamp(x1 seconds),Downloaded cunks,Total Downloaded chunks,Messages count,Total messages count,Completed,Total completed'
for t in tab:
    if (i==0):
        tab2[i] = tab[i]
        message_count[i] = messages[i]
        completed_acc[i] = completed[i]
    else:
        tab2[i] = tab2[i-1]+tab[i]
        message_count[i] = messages[i] + message_count[i-1]
        completed_acc[i] = completed_acc[i-1] + completed[i]

    #print i,",",t,",",tab2[i],",",messages[i],",",message_count[i],",",completed[i],",",completed_acc[i]
    f1.write( str(i)+","+str(t)+","+str(tab2[i])+","+str(messages[i])+","+str(message_count[i])+","+str(completed[i])+","+str(completed_acc[i])+"\n")
    i+=1

f1.close()


f2 = open(dest+os.sep+no_ext+"-activity.csv", 'wb')
f2.write('Time(s),active,inactive\n')
for a in activity_logs:
    #print i,",",t,",",tab2[i],",",messages[i],",",message_count[i],",",completed[i],",",completed_acc[i]
    f2.write(str(int(a[0])/1000)+","+a[1]+","+a[2]+"\n" )

f2.close()



f3 = open(dest+os.sep+no_ext+"-node_activity.csv", 'wb')
f3.write('Node,Transf. start(s),Transf. end(s)\n')
for a in activity_segs:
    #print i,",",t,",",tab2[i],",",messages[i],",",message_count[i],",",completed[i],",",completed_acc[i]
    f3.write( a[0]+","+str(int(a[1])/1000)+","+str(int(a[2])/1000)+"\n" )
f3.close()


#Node stats
f4 = open(dest+os.sep+no_ext+"-node_stats.csv", 'wb')
f4.write('Key,Active Time(s),Inactive time(s)\n')
for key in node_stats.keys():
    f4.write( str(key)+","+str(node_stats[key]["ACTIVE_TIME"]/1000)+","+str(node_stats[key]["SLEEP_TIME"]/1000)+"\n" )
f4.close()
