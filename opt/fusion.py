#! /usr/bin/python
import pandas as pd
import os
import sys
if len(sys.argv) < 4:
	print "Error: requires an argument <Output file> <Input files>"
	exit(0)

output= sys.argv[1]
print "Fusion of:   ",sys.argv[2::]
files = sys.argv[2::]

dataframes = [pd.read_csv(filename,sep=',') for filename in files]
lengths= [len(df) for df in dataframes]
max_index= lengths[lengths.index(max(lengths))]
#dataframes[max_index].index= dataframes[max_index][dataframes[max_index].columns[0]]
res = pd.concat(dataframes, axis=1)
res = res.fillna(method='ffill')
res.to_csv(output)
