#! /bin/env/python
import os
import sys
import pandas as pd
def show_usage():
    print "Usage:"
    print "python total_completed_node_mean.py <file1> <file2> <file3>"



if len(sys.argv) < 4:
    print "Error: not enough arguments"
    show_usage()
    exit(0)


filepaths = sys.argv[2::]

dfs = [pd.read_csv(f,sep=",") for f in filepaths]
max_v = -1
max_i = 0
cur = 0
for df in dfs:
    df.index=df["Time(s)"]
    if (len(df.index) > max_v):
        max_v = len(df.index)
        max_i = cur
    cur = cur + 1



total_completed = [df["Total completed"] for df in dfs]

df = pd.concat(total_completed,axis=1)
df = df.fillna(method='ffill')
print df
df = df.mean(axis=1)
df.index = dfs[max_i].index
df.to_csv(sys.argv[1],sep=",")


#print total_completed
