#! /bin/bash
echo $#
echo $1
if [ $# -lt 1 ]; then
  echo "Need a folder path as an argument"
  exit 0
fi
CURDIR=`pwd`
mkdir "$1"
cd "$1"
DIR=`pwd`
echo $DIR
rm -rf out
mkdir out
#network-size
for i in *.out ; do
  echo "Writing $(basename $i .out).csv"
  python $CURDIR/analyse.py "$i" out
  
done;
cd $CURDIR
