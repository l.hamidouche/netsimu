VER=1.0.4

.PHONY: all run clean doc release

all:
	javac -classpath src:lib/jep-2.3.0.jar:lib/djep-1.0.0.jar:lib/peersim-1.0.5.jar:lib/log4j-core-2.5.jar:lib/log4j-api-2.5.jar `find src -name "*.java"` -d bin

run:
	@java -Dlog4j.configurationFile="/Users/magency/Documents/lyes/dev/netsimu/log4j.configurationFile" -classpath bin:src:lib/jep-2.3.0.jar:lib/djep-1.0.0.jar:lib/peersim-1.0.5.jar:lib/log4j-core-2.5.jar:lib/log4j-api-2.5.jar peersim/Simulator opt/config2.cfg

run-no-log:
	@java -Dlog4j.configurationFile="/Users/magency/Documents/lyes/dev/netsimu/log4j.configurationFile.off" -classpath bin:src:lib/jep-2.3.0.jar:lib/djep-1.0.0.jar:lib/peersim-1.0.5.jar:lib/log4j-core-2.5.jar:lib/log4j-api-2.5.jar peersim/Simulator opt/config2.cfg


clean:
	rm -f `find . -name "*.class"`

doc:
	rm -rf doc/*
	javadoc -overview overview.html -classpath src:lib/jep-2.3.0.jar:lib/djep-1.0.0.jar::lib/peersim-doclet.jar -d doc \
                -group "Peersim" "peersim*" \
                -group "Examples" "example.*" \
		peersim \
		peersim.cdsim \
		peersim.config \
		peersim.core \
		peersim.dynamics \
		peersim.edsim \
		peersim.graph \
		peersim.rangesim \
		peersim.reports \
		peersim.transport \
		peersim.util \
		peersim.vector \
		example.aggregation \
		example.loadbalance \
		example.edaggregation \
		example.hot \
		example.newscast

docnew:
	rm -rf doc/*
	javadoc -overview overview.html -docletpath peersim-doclet.jar -doclet peersim.tools.doclets.standard.Standard -classpath src:jep-2.3.0.jar:djep-1.0.0.jar -d doc \
                -group "Peersim" "peersim*" \
                -group "Examples" "example.*" \
		peersim \
		peersim.cdsim \
		peersim.config \
		peersim.core \
		peersim.dynamics \
		peersim.edsim \
		peersim.graph \
		peersim.rangesim \
		peersim.reports \
		peersim.transport \
		peersim.util \
		peersim.vector \
		example.aggregation \
		example.loadbalance \
		example.hot \
		example.edaggregation \
		example.newscast


release: clean all docnew
	rm -fr peersim-$(VER)
	mkdir peersim-$(VER)
	cp -r doc peersim-$(VER)
	cp Makefile overview.html README CHANGELOG RELEASE-NOTES build.xml peersim-doclet.jar peersim-$(VER)
	mkdir peersim-$(VER)/example
	cp example/*.txt peersim-$(VER)/example
	mkdir peersim-$(VER)/src
	cp --parents `find src/peersim src/example -name "*.java"` peersim-$(VER)
	cd src ; jar cf ../peersim-$(VER).jar `find peersim example -name "*.class"`
	mv peersim-$(VER).jar peersim-$(VER)
	cp jep-2.3.0.jar peersim-$(VER)
	cp djep-1.0.0.jar peersim-$(VER)
